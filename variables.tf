variable "name" {
  description = "Name of backup plan"
  type        = "string"
}

variable "selection_tag" {
  description = "List of tags to attribute resources"
  default     = []
}

variable "resources" {
  description = "List of resources id to attribute resources"
  default     = []
}

variable "backup_plan_id" {
  description = "ID of backup plan"
  type        = "string"
}

variable "iam_role_arn" {
  description = "Role"
  type        = "string"
}
