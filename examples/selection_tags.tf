module "backup_plan_resource-NAME" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-backup-resource.git?ref=master"
  name           = "NAME"
  backup_plan_id = "${module.backup_plan-NAME.plan_id}"
  iam_role_arn   = "${module.backup_plan-NAME.role_arn}"

  selection_tag = [
    {
      type  = "STRINGEQUALS"
      key   = "Name"
      value = "vpn"
    },
  ]
}
