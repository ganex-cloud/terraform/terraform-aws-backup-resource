resource "aws_backup_selection" "default" {
  plan_id       = "${var.backup_plan_id}"
  name          = "${var.name}"
  iam_role_arn  = "${var.iam_role_arn}"
  selection_tag = ["${var.selection_tag}"]
  resources     = ["${var.resources}"]
}
